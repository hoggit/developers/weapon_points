package.path = "../".. "?.lua;" .. package.path

function fixtures()
    lfs = {}
    lfs.writedir = function() return "." end
    mist = {}
    mist.Logger = {
        new = function(name, level)
            return {
                info = function(str)
                    print("LOG -- " .. name .. " - " .. str)
                end
            }
        end
    }
    mist.makeUnitTable = function(config)
        return {}
    end
    mist.addEventHandler = function(handler)
    end
    mist.scheduleFunction = function(cb, args,time,rep)
    end
    mist.utils = {
        deepCopy = function(tbl) return tbl end
    }
    timer = {}
    timer.getTime = function() return 0 end
end
fixtures()

local test = require("u-test")
local wp = require "weapon_points"

local Weapon = function( make )
     return function (count)
        return {
            ["desc"] = {
                ["displayName"] = make
            },
            ["count"] = count
        }
    end
end

local Ammo = {
    aim120c = Weapon("AIM-120C"),
    aim120b = Weapon("AIM-120B"),
    sd10 = Weapon("SD-10"),
    gbu12 = Weapon("GBU-12")
}

local t = test.points_for_ammo

t.should_be_able_to_calculate_points_on_a_single_weapon = function()
    local weapon_aim120 = Ammo.aim120c(1)
    test.equal(wp.PointsForAmmo({weapon_aim120})["Air"], 5)
    test.equal(wp.PointsForAmmo({weapon_aim120})["Ground"], nil)
end

t.should_be_able_to_calculate_points_on_multiple_of_the_same_weapon = function()
    local weapon_aim120 = Ammo.aim120c(5)
    test.equal(wp.PointsForAmmo({weapon_aim120})["Air"], 25)
    test.equal(wp.PointsForAmmo({weapon_aim120})["Ground"], nil)
end

t.should_be_able_to_calc_multiple_weapons_in_air_category = function()
    local weapon_aim120 = Ammo.aim120c(5)
    local weapon_sd10 = Ammo.sd10(2)
    local points = wp.PointsForAmmo({weapon_aim120, weapon_sd10})
    test.equal(points["Air"], 35)
end

t.should_be_able_to_calc_multiple_weapons_in_different_categories = function()
    local weapon_sd10 = Ammo.sd10(2)
    local weapon_gbu12 = Ammo.gbu12(4)
    local points = wp.PointsForAmmo({weapon_gbu12, weapon_sd10})
    test.equal(points["Air"], 10)
    test.equal(points["Ground"], 20)
end

t = test.maxPoints

t.should_provide_defaults_for_airframes = function()
    local fakeUnit = {
        getTypeName = function() return "NOT A PLANE LOLOLOL" end
    }
    local maxPoints = WeaponPoints.GetMaxPoints(fakeUnit)
    test.equal(maxPoints["Air"], 30)
    test.equal(maxPoints["Ground"], 60)
end

t.should_use_overrides_if_available = function()
    WeaponPoints.AirframeOverrides["TEST_PLZ_IGNORE"] = {["Air"] = 150, ["Ground"] = 999, ["FakeCategory"] = 1234}
    local fakeUnit = {
        getTypeName = function() return "TEST_PLZ_IGNORE" end
    }
    local maxPoints = wp.GetMaxPoints(fakeUnit)
    test.equal(maxPoints["Air"], 150)
    test.equal(maxPoints["Ground"], 999)
    test.equal(maxPoints["FakeCategory"], 1234)
end
